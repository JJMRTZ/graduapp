import React from 'react'
import '../Styles/registro.css'
import TextField from "@material-ui/core/TextField";
import Modal from '../Componentes/Modal'
import {database} from "../firebase";
import { useState } from 'react';
import {
    FormControl,
    InputLabel,
    FilledInput,
    Select,
    MenuItem,
  } from '@material-ui/core';

const Registro = () =>{
 
    const [State, setState] = useState({
        nombre: {
            value: '',
            required: true,
            error: false,
            label: 'Nombre(s)'
          },
          paterno: {
            value: '',
            required: true,
            error: false,
            label: 'Apeido Paterno'
          },
          materno: {
            value: '',
            required: true,
            error: false,
            label: 'Apeido Materno'
          },
          correo: {
            value: '',
            required: true,
            error: false,
            label: 'Correo Electronico'
          },
          carrera: {
            value: '',
            required: true,
            error: false,
            label: 'Elija una Carrera',
            name: 'carrera',
          },
          control: {
            value: '',
            required: true,
            error: false,
            label: 'Numero de Control'
          },
          contraseña: {
            value: '',
            required: true,
            error: false,
            label: 'Contraseña',
          },
          veripass: {
            value: '',
            required: true,
            error: false,
            label: 'Repita su contraseña',
          },
          ModalRegistro: false,
  
      });

      const handleCloseModal = e =>{
        setState({ 
            ...State,
            ModalRegistro: false,
        });
      }

     

      const [carreras] = useState({
        Ingenierias: {
         Sistemas: 'Sistemas',
         Industrial: 'Industrial',
         Gestion: 'Gestion',
         Bio: 'Bioquimica',
         electrica: 'Electrica',
         electronica: 'Electronica',
         mecanica: 'Mecanica',
         mecatro: 'Mecatronica',
         }
        });

        const checkRegExp = (obj, regExp, label, labelDefault, size) => {
            if (obj.value === null) {
                obj.value = '';
            }
            var longitud;
            var expresion;
      
            obj.error = (obj.value.length > size);
            longitud = obj.error;
            obj.error = !(regExp.test(obj.value));
            expresion = obj.error;
            if (longitud) {
                obj.label = `La longitud maxima es de ${size} caracteres`;
            } else if (expresion) {
                obj.label = label;
            } else {
                obj.label = labelDefault;
            }
      
            return obj;
         }

         const checkPass = (obj, obj2, label, labelDefault) =>{
                if(obj.value != obj2.value){
                  obj.label = label;
                  obj.error = true;
                }else{
                  obj.label = labelDefault;
                }

            return obj;
         }

        const  handleChange = (e) => {
            let obj = null;
            let obj22 = null;
            obj = State.nombre;
            const { name, value } = e.target;
            switch (name) {
                
                case 'Nombre':
                  obj.value = value; 
                  obj = checkRegExp(obj,/^[a-zA-Z ]*$/,'Nombre no valido','Nombre',10);
                  setState( {...State, nombre: obj})
                break;
    
                case 'Correo':
                  obj = State.correo;
                  obj.value = value; 
                  obj = checkRegExp(obj,
                      /^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/,
                      'Correo no valido',
                      'Correo');
                  setState({...State, correo: obj });
                  break;
    
                  case 'Paterno':
                  obj = State.paterno;
                  obj.value = value; 
                  obj = checkRegExp(obj,
                       /^[a-zA-Z ]*$/,
                      'Apeido no valido',
                      'Apeido Paterno',10);
                  setState({...State, paterno: obj });
                  break;
    
                  case 'Materno':
                  obj = State.materno;
                  obj.value = value; 
                  obj = checkRegExp(obj,
                       /^[a-zA-Z ]*$/,
                      'Apeido no valido',
                      'Apeido Materno',10);
                  setState({...State, materno: obj });
                  break;
    
                    
                    case 'Control':
                    obj = State.control;
                    obj.value = value;
                    obj = checkRegExp(obj,
                      /^([0-9]){8}/,
                     'Numero de control no valido',
                     'Numero de control',10);
                    setState({...State, control: obj });
                    break;
    
                    case 'carrera':
                    obj = State.carrera;
                    obj.value = value; 
                    setState({...State, carrera: obj });
                    break;
    
                    case 'Contraseña':
                    obj = State.contraseña;
                    obj.value = value;
                    setState({...State, contraseña: obj });
                    break;

                     case 'Verificapass':
                     obj = State.veripass;
                     obj22 = State.contraseña
                     obj.value = value;
                     obj = checkPass(obj, obj22,'Contraseña no coincide', 'Contraseña Correcta');
                     setState({...State, veripass: obj });
                     break;

            }
        }

        const handleEnvio = () =>{
           var referencia = database.ref('/estudiantes');
           referencia.push({
              ap_m: State.materno.value,
              ap_p: State.paterno.value,
              carrera: State.carrera.value,
              contraseña: State.contraseña.value,
              correo: State.correo.value,
              n_control: State.control.value ,
              nombre: State.nombre.value,
            });

            setState({ 
              ...State,
              ModalRegistro: true,
          });

        }

        const vacio = () =>{
          if(State.nombre.value === ''){
              State.nombre.error = true;
          }
          if(State.paterno.value === ''){
            State.paterno.error = true;
          }
          if(State.materno.value === ''){
            State.materno.error = true;
          }
          if(State.correo.value === ''){
            State.correo.error = true;
          }
          if(State.carrera.value === ''){
            State.carrera.error = true;
          }else{
            State.carrera.error = false;
          }
          if(State.control.value === ''){
            State.control.error = true;
          }
          if(State.veripass.value === ''){
            State.veripass.error = true;
          }else{
            State.veripass.error = false;
          }

        }

        return(
            <div className="container">
                <div className="form">
                <div className="title">
                    <p className="text-1">Registrar</p>
                    <p className="text-2">Ingresa los siguientes datos para crear una cuenta</p>
                </div>
                  {vacio()}
               <div className="row">
               <div className="col-md-6">
                     <TextField
                    label={State.nombre.label}
                    margin="normal"
                    name="Nombre"
                    className="cont-color"
                    value={State.nombre.value}
                    required={State.nombre.required}
                    onChange={handleChange}
                   />
                </div >
                <div className="col-md-6">
                <TextField
                  label={State.paterno.label}
                  margin="normal"
                  name="Paterno"
                  className="cont-color"
                  value={State.paterno.value}
                  required={State.paterno.required}
                  onChange={handleChange}
                />
                </div>
                </div>
 
                <div className="row">
                <div className="col-md-6">
                    <TextField
                    label={State.materno.label}
                    margin="normal"
                    name="Materno"
                    className="cont-color"
                    value={State.materno.value}
                    required={State.materno.required}
                    onChange={handleChange}
                    />
                </div>
                <div className="col-md-6">
                <TextField
                  label={State.control.label}
                  margin="normal"
                  name="Control"
                  className="cont-color"
                  value={State.control.value}
                  required={State.control.required}
                  onChange={handleChange}
                  />
                </div>
                </div>
                
                <div className="row">
                <div className="col-md-6 carreras" >
                    <FormControl className="cont-color" variant="filled">
                          <InputLabel htmlFor={`filled-${State.carrera.name}-simple`}>{State.carrera.label}</InputLabel>
                           <Select
                             value={State.carrera.value}
                             onChange={handleChange}
                             required={State.carrera.required}
                             input={<FilledInput name={State.carrera.name} id={`filled-${State.carrera.name}-simple`} fullWidth={true} />}
                            >
                            {Object.entries(carreras.Ingenierias).map((item, index) => {
                                return <MenuItem key={index} value={item[1]}>{item[1]}</MenuItem>
                            })}
                            </Select>
                    </FormControl>
                 </div>

                <div className="col-md-6">
                <TextField
                  label={State.correo.label}
                  margin="normal"
                  name="Correo"
                  className="cont-color"
                  value={State.correo.value}
                  required={State.correo.required}
                  onChange={handleChange}
                  />
                </div>
                </div>

                <div className="row">
                <div className="col-md-6">
                    <TextField
                        label={State.contraseña.label}
                        margin="normal"
                        name="Contraseña"
                        type="password"
                        className="cont-color"
                        value={State.contraseña.value}
                        required={State.contraseña.required}
                        onChange={handleChange}
                    />

                </div>
                
                <div className="col-md-6">
                <TextField
                  label={State.veripass.label}
                  margin="normal"
                  name="Verificapass"
                  type="password"
                  className="cont-color"
                  value={State.veripass.value}
                  required={State.veripass.required}
                  onChange={handleChange}
                 />
                </div>
                </div>
                
                 <button 
                    className="btn-Enviar"
                    onClick={handleEnvio}
                    disabled={ State.nombre.error || State.paterno.error || State.materno.error
                      || State.control.error || State.carrera.error || State.correo.error
                      || State.veripass.error  }
    
                    > 
                Registrar </button>
                <Modal 
                 isOpen={State.ModalRegistro} 
                 onClose={handleCloseModal}>     
                  <div className="contenedor-11">
                    <button  onClick={handleCloseModal} className="btn-Enviar11" handleChange={handleCloseModal} >
                        Aceptar
                    </button>
                  </div>  
                </Modal>
                </div>
            </div>
        )
}

export default Registro;