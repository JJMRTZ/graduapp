import React, {useState, Fragment} from 'react'
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Dialog from '@material-ui/core/Dialog';
import TextField from '@material-ui/core/TextField';
import { database } from "../firebase";

const Tarjeta = (props) => {
    const [open,setOpen]= useState(false);
    const [open2,setOpen2]= useState(false);
    const [values, setValues] = useState({
        reason:'',
    });

    const handleChange = prop => event => {
        setValues({ ...values, [prop]: event.target.value });
    };
    const handleOpen= e=>{
        e.preventDefault();
        setOpen(true);
        console.log(open);
    }

    const handleClose = e =>{
        setOpen(false);
 
        var referencia = database.ref('/solicitudes/'+props.solicitud);
        referencia.update({
           estado: 1,
         });
           
         /*var doc = new jsPDF();
            doc.text(50, 15, 'INSTITUTO TECNOLOGICO DE MORELIA');
            doc.text(65, 30, 'CEREMONIA DE GRADUCACION');
            doc.text(25, 50, 'Le brindamos el honor a usted '+' nombre completo del alumno');
            doc.text(25, 56, 'de presentarse en la ceremonia de graduacion la cual se');
            doc.text(25, 62, 'llevara acabo en el auditorio del instituto tecnologico de morelia');
            doc.text(25, 68, 'en el horario de '+ 'Horario asignado del alumno');
            doc.text(25, 80, 'PROGRAMA: ');
            doc.text(25, 90, '1.- PRESENTACION DE LOS GRADUADOS');
            doc.text(25, 100, '2.- HONORES A LA BANDERA');
            doc.text(25, 110, '3.- PALABRAS DE NUESTRO DIRECTOR INSTITUCIONAL');
            doc.text(25, 120, '4.- ENTREGA DE CERTIFICADOS');
            doc.text(25, 130, '5.- ENTREGA DE RECONOCIMIENTOS ESPECIALES');
            doc.text(25, 140, '6.- DISCURSO DE GRADUACION');
            doc.text(25, 150, '7.- PRESENTACION DEL VIDEO ECHO POR LOS ALUMNOS');
            doc.text(25, 160, '8.- ENTREGA DE DIPLOMAS A LOS ALUMNOS DESTACADOS');
            doc.text(25, 170, '9.- CIERRE Y DESPEDIDA');
        
            doc.text(25, 190, 'De antemano le prindamos a usted'+'nombre del alumno completo');
            doc.text(25, 200, 'cuya carrera terminada es'+'nombre complento de la carrera');
            doc.text(25, 210, 'muchas felicidades por su logro deseandole asi que continue ');
            doc.text(25, 220,  'y siga aspirando a mas logros, lo esperamos en la ceremonia');
            
            doc.text(25, 280, 'LA EDUCACION ES EL ARMA MAS PODEROSA QUE PUEDES');
            doc.text(55, 290, 'USAR PARA CAMBIAR LE MUNDO');
        
           // doc.save('prueba.pdf');
            const file = doc;
            console.log("archivo",doc);
            const storageRef = firebase.storage().ref('/Pdfs/');
            const task = storageRef.put(doc);
            console.log("Archivo" + file);
           */
            
    }

    const handleClose2 = e =>{
        setOpen2(false);

        var referencia = database.ref('/solicitudes/'+props.solicitud);
        referencia.update({
           estado: 2,
           notas: values.reason,
         });


    }
    const handleReason = e =>{
        setOpen(false);
        setOpen2(true);
    }
    const handleReturn = e =>{
        setOpen2(false);
        setOpen(true);
    }

    return(
        <Fragment>

        <div>
                      <Fragment>
                        <Card style={{height:"fit-content",marginTop:"2%",marginLeft:"2%",width:"250px"}}>
                        <CardContent>
                            <Typography style={{fontSize:"large"}}>
                                {props.nombre}
                            </Typography>
                            <Typography >
                                Número de control: {props.control}
                            </Typography>
                            <Typography >
                                Carrera: {props.carrera}
                            </Typography>
                        </CardContent>
                        <CardActions>
                            <Button size="small" style={{color:"#0097A7",fontWeight:"bold"}} onClick={handleOpen}>Ver</Button>
                        </CardActions>
                     </Card>
                    </Fragment>
                  
                
             </div>
           
            <Dialog aria-labelledby="simple-dialog-title" open={open} style={{width:"100%"}} >

                 <div style={{alignSelf:"center",marginTop:"3%"}}>
                <Typography style={{fontWeight:"bold",fontSize:"large"}}>
                    {props.nombre} {props.paterno} {props.materno}
                </Typography>
                <Typography>
                    Numero de control: {props.control}
                </Typography>
                <Typography>
                    Carrera: {props.carrera}
                </Typography>
                </div>
                <img src={props.foto} alt="" width="35%" style={{alignSelf:"center",margin:"auto",marginBottom:"20%",marginTop:"20%"}}/>
                <div style={{marginBottom:"4%"}}>
                    <Button variant="contained" style={{backgroundColor:"#FFC107",color:"white",float:"right",marginRight:"4%"}} onClick={handleClose} >
                        Aceptar Pago
                    </Button>
                    <Button style={{color:"#FFC107",float:"right"}} onClick={handleReason}>Rechazar Pago</Button>
                </div>
             </Dialog>
             <Dialog aria-labelledby="simple-dialog-title" open={open2} style={{width:"100%"}} >
                 <div style={{alignSelf:"center",marginTop:"3%",margin:"8%",textAlign:"center",marginBottom:"4%"}}>
                <Typography style={{fontWeight:"bold",fontSize:"large"}}>
                   Rechazar pago
                </Typography>
                <Typography>
                    Ingrese a continuación la razón del rechazo del pago
                </Typography>
                </div>
                <TextField
                        id="filled-helperText"
                        label="Razón"
                        helperText="Razón del rechazo del pago"
                        variant="filled"
                        style={{
                            width: "92%",
                            alignSelf: "center",
                            marginTop: "4vh"
                        }}
                        onChange={handleChange('reason')}
                    />
                <div style={{marginBottom:"4%",marginTop:"4%"}}>
                    <Button  variant="contained"  style={{backgroundColor:"#FFC107",color:"white",float:"right",marginRight:"8%"}} onClick={handleClose2} >
                        Rechazar Pago
                    </Button>
                    <Button style={{color:"#FFC107",float:"right"}} onClick={handleReturn}>Regesar</Button>
                </div>
             </Dialog>
        </Fragment>
    )
}

export default Tarjeta;
