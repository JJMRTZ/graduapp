import React,{useState, Fragment} from 'react';
import AppBar from '../Components/AppBar';
import SideMenu from '../Components/SideMenu';
import Tarjeta from './Card';
import { database } from "../firebase";

export default function Admin(){
      
    const [Estudiantes, setEstudiantes] = useState({});
    const [Solicitudes, setSolicitudes] = useState({});
    var Estu = [];
    var Control = [];
    var id_Solicitud = [];
    var UrlFoto = [];

    database.ref("/estudiantes").on("value", snapshot => {
        const val = snapshot.val();
        if (Object.keys(val).length !== Object.keys(Estudiantes).length) {
          setEstudiantes(val);
        }
      });

    database.ref("/solicitudes").on("value", snapshot => {
        const val = snapshot.val();
        if (Object.keys(val).length !== Object.keys(Solicitudes).length) {
          setSolicitudes(val);
        }
      });

    const handleGet = () =>{
        var N_Control = [];
        var x;
        {Object.keys(Solicitudes).map((key) => {
                if(Solicitudes[key].estado === 0){
                    N_Control.push(Solicitudes[key].n_control);
                    id_Solicitud.push(key);
                    UrlFoto.push(Solicitudes[key].foto);
                }
        })}
        {Object.keys(Estudiantes).map((key) => {
            for(x=0; x<N_Control.length; x++){
                if(N_Control[x] === key){
                    Control.push(key);
                    Estu.push(Estudiantes[key]);
                }
            }
        })}

    }  


    return(
        <div style={{height:"100vh",display:"flex",flexDirection:"column",flexWrap:"nowrap"}}>
             {handleGet()}
            <AppBar nombre="GraduApp"/>
            <div style={{display:"flex",flexDirection:"row",width:"100%",height:"100%"}}>
            <SideMenu select="solicitudes" />
            <div style={{display:"flex",flexDirection:"row",backgroundColor:"#F4F4F4",justifyContent:"space-around",flexWrap:"wrap",width:"100%",height:"100%"}}>
                {Object.keys(Estu).map((key) => {
                    return (
                        <Tarjeta 
                        nombre={Estu[key].nombre} 
                        control={Control[key]} 
                        carrera={Estu[key].carrera}
                        materno={Estu[key].ap_m} 
                        paterno={Estu[key].ap_p}
                        solicitud={id_Solicitud[key]}
                        foto={UrlFoto[key]}
                           />
                        );
                
                    })}
                        
            </div>   
            </div>
        </div>
    );
}