import React from 'react';
import ReactDom from 'react-dom';
import '../Styles/Modal.css';

function Modal (props){
    if(!props.isOpen){
        return null;
    }
    
return ReactDom.createPortal(
    <div className="Modal100">
        <div className="Modal_container100">
            <div className="tittle100">
              Registro Exitoso!!
            </div>
            <div>
                {props.children}
            </div>
        </div>
    </div>,
    document.getElementById('Modal')
);
}

export default Modal;