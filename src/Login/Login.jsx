import React, { useState } from 'react';
import {useHistory} from 'react-router-dom';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import FilledInput from '@material-ui/core/FilledInput';
import InputLabel from '@material-ui/core/InputLabel';
import InputAdornment from '@material-ui/core/InputAdornment';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import IconButton from '@material-ui/core/IconButton';
import FormControl from '@material-ui/core/FormControl';
import FormHelperText from '@material-ui/core/FormHelperText';
import Button from '@material-ui/core/Button';
import clsx from 'clsx';
import HomeImage from '../Images/img_home.jpg';
import Logo from '../Images/img_logo.png';
import firebase from '../firebase';

const useStyles = makeStyles(theme => ({
    root: {
        display: "flex",
        flexDirection: "row",
        width: "100%",
        height: "100vh",
    },
    izquierda: {
        width: "50%",
        backgroundImage: `linear-gradient(
            rgba(25,118,210, .7),
            rgba(25,118,210, .7)
          ),url(${HomeImage})`,
        height: "100%",
        backgroundRepeat: "no-repeat",
        backgroundSize: "cover",
        alignContent: "center",
        justifyContent: "center",
        display: "flex",
        flexDirection: "column",
        [theme.breakpoints.down(750)]:{
            display:"none",
        }
    },
    derecha: {
        width: "50%",
        backgroundColor: "white",
        height: "100%",
        alignContent: "center",
        justifyContent: "center",
        display: "flex",
        flexDirection: "column",
        [theme.breakpoints.down(750)]:{
            width:"100%",
        }
    },
    margin: {
        margin: "auto",
        width: "100%",
        marginTop: "4%"
    },
    imgres:{
        display:"none",
        width:"25%",
        alignSelf: "center",
        [theme.breakpoints.down(750)]:{
            display:"block",
        }
    },
    btnres:{
        backgroundColor:"#FFC107",
        color:"white",
        marginTop:"1vh",
        width:"100%",
        alignSelf:"center",
        marginBottom:"8vh",
        display:"none", 
        [theme.breakpoints.down(750)]:{
            display:"block",
        } 
    }
}));

export default function Login() {
    const classes = useStyles();
    const history = useHistory();
    const [values, setValues] = useState({
        password: '',
        usuario: '',
        showPassword: false,
    });
    const handleChange = prop => event => {
        setValues({ ...values, [prop]: event.target.value });
    };

    const handleClickShowPassword = () => {
        setValues({ ...values, showPassword: !values.showPassword });
    };

    const handleMouseDownPassword = event => {
        event.preventDefault();
    };
    const handleSubmit = e => {
        var check=false;
        e.preventDefault();
        var ref = firebase.database().ref("/admins/");
        const promesa =ref.once("value")
        .then(function(snapshot){
            const val = snapshot.val();
            if(val!== null){
                Object.keys(val).map(key => {
                    if (val[key].usuario === values.usuario && val[key].password === values.password){
                        check=true;
                        history.push("/Admin")
                    }
                });
            }
            if(check===false){
                var ref = firebase.database().ref("/estudiantes/"+values.usuario);
                ref.once("value")
                .then(function(snapshot){
                    const val = snapshot.val();
                    if(val!== null){
                        if(val.contraseña === values.password){
                            var myObj = {id: values.usuario, nombre: val.nombre+" "+val.ap_p+" " + val.ap_m, carrera: val.carrera, solicitud: val.solicitud};            
                            localStorage.setItem("usuario", JSON.stringify(myObj));
                            if(val.solicitud !== "null"){
                                
                                var ref = firebase.database().ref("/solicitudes/"+val.solicitud)
                                ref.once("value")
                                .then(function(snapshot){
                                    const val = snapshot.val();
                                    if(val.estado === 0){
                                        history.push(`/Procesando`)
                                    }
                                    else if(val.estado === 1){
                                        history.push(`/Invitacion`)
                                    }
                                    else {
                                        history.push(`/Subir`)
                                    }
                                    return val;
                            });
                            }
                            else{
                                history.push("/Subir")
                            }
                        }else{
                            window.alert("Contraseña o usuario incorrecto")
                        }
                    }else{
                        window.alert("Contraseña o usuario incorrecto")
                    }
    
                return val; 
                });
            }
            return val; 
        });
    }
    return (
        <div className={classes.root}>
            <div className={classes.izquierda}>
                <img src={Logo} alt="" width="25%" style={{ alignSelf: "center"}} />
                <Typography style={{ fontSize: "larger", textAlign: "center",color:"white",marginTop:"1vh"}}>
                    GraduApp
                </Typography>
                <Typography style={{ fontSize: "large", textAlign: "center",color:"white",marginTop:"8vh"}}>
                    ¿No tienes cuenta?<br/>
                    Por favor crea una cuenta para poder <br/>
                    atender tu solicitud
                </Typography>
                <Button variant="contained" style={{backgroundColor:"#FFC107",color:"white",marginTop:"1vh",width:"46%",alignSelf:"center",marginBottom:"8vh"}} onClick={e =>history.push("/Regist")}>
                        Crear cuenta
                </Button>
            </div>
            <div className={classes.derecha}>
            <img src={Logo} alt="" className={classes.imgres}/>
                <Typography style={{ fontWeight: "bold", fontSize: "larger", textAlign: "center" }}>
                    Iniciar Sesión
             </Typography>
                <Typography style={{
                    fontSize: "large",
                    textAlign: "center",
                    marginBottom: "4vh",
                }}
                    color="textSecondary">
                    Ingresa tus credenciales para iniciar sesión
             </Typography>
                <form style={{
                    width: "60%",
                    alignSelf: "center",
                }}
                    onSubmit={handleSubmit}>
                    <TextField
                        id="filled-helperText"
                        label="Usuario"
                        helperText="Ingresa tu usuario"
                        variant="filled"
                        style={{
                            width: "100%",
                            alignSelf: "center",
                            marginTop: "4vh"
                        }}
                        onChange={handleChange('usuario')}
                    />
                    <FormControl className={clsx(classes.margin, classes.textField)} variant="filled">
                        <InputLabel htmlFor="filled-adornment-password">Password</InputLabel>
                        <FilledInput
                            id="filled-adornment-password"
                            type={values.showPassword ? 'text' : 'password'}
                            value={values.password}
                            onChange={handleChange('password')}
                            endAdornment={
                                <InputAdornment position="end">
                                    <IconButton
                                        aria-label="toggle password visibility"
                                        onClick={handleClickShowPassword}
                                        onMouseDown={handleMouseDownPassword}
                                        edge="end"
                                    >
                                        {values.showPassword ? <Visibility /> : <VisibilityOff />}
                                    </IconButton>
                                </InputAdornment>
                            }
                        />
                        <FormHelperText id="standard-weight-helper-text">Ingresa la contraseña</FormHelperText>
                    </FormControl>
                    <Button variant="contained" type="submit" style={{backgroundColor:"#FFC107",color:"white",marginTop:"1vh",width:"100%"}}>
                        Login
                    </Button>
                    <Button variant="contained" className={classes.btnres} onClick={e =>history.push("/Regist")}>
                        Crear cuenta
                    </Button>
                </form>
            </div>
        </div>
    );
}