import React from "react";
import SideMenu from "../Components/SideMenu";
import AppBar from "../Components/AppBar";
import { Chart } from "react-google-charts";
import Select from '@material-ui/core/Select';
import FormControl from '@material-ui/core/FormControl';
import FormHelperText from '@material-ui/core/FormHelperText';
import Typography from '@material-ui/core/Typography';
import { MenuItem } from "@material-ui/core";
import InputLabel from '@material-ui/core/InputLabel';

/*function getData(periodo){
    const data =[];

    return data;
}*/

export default function Charts() {
    const [periodo, setPeriodo] = React.useState();
    return (
        <div
            style={{
                height: "100vh",
                display: "flex",
                flexDirection: "column",
                flexWrap: "nowrap"
            }}
        >
            <AppBar nombre="GraduApp" />
            <div
                style={{
                    display: "flex",
                    flexDirection: "row",
                    width: "100%",
                    height: "100%"
                }}
            >
                <SideMenu select="estadisticas" />
                <div
                    style={{
                        display: "flex",
                        flexDirection: "column",
                        backgroundColor: "#F4F4F4",
                        flexWrap: "wrap",
                        width: "100%",
                        height: "100%"
                    }}
                >
                    <Typography style={{ fontWeight: "bold", fontSize: "xx-large", alignSelf: "center" }}>
                        Gráficas de graduados por carrera
        </Typography>
                    <FormControl variant="filled" style={{ marginBottom: "2%", width: "30%",marginLeft:"3%",marginTop:"2vh" }}>
                        <InputLabel id="demo-simple-select-filled-label">Periodo</InputLabel>
                        <Select
                            labelId="demo-simple-select-filled-label"
                            id="demo-simple-select-filled"
                            name="Periodo"
                            value={periodo}
                            onChange={e => { setPeriodo(e.target.value) }}
                        >
                            <MenuItem value="">
                                <em>None</em>
                            </MenuItem>
                            <MenuItem value={"Enero-Junio(2020)"}>Enero-Junio(2020)</MenuItem>
                            <MenuItem value={"Agosto-Diciembre(2019)"}>Agosto-Diciembre(2019)</MenuItem>
                        </Select>
                        <FormHelperText id="standard-weight-helper-text">Seleccione su carrera</FormHelperText>
                    </FormControl>
                    <Chart
                        style={{ alignSelf: "center", marginTop: "2vh", fill: "transparent" }}
                        width={"800px"}
                        height={"500px"}
                        chartType="PieChart"
                        loader={<div>Loading Chart</div>}
                        data={[
                            ["Carrera", "Graduados"],
                            ["Ingeniería en sistemas", 33],
                            ["Gestión empresarial ", 26],
                            ["Electrica", 22],
                            ["Electrónica", 10],
                            ["Industrial", 9]
                        ]}
                        options={{

                            backgroundColor: "transparent",
                        }}
                        rootProps={{ "data-testid": "5" }}
                    />
                </div>
            </div>
        </div>
    );
}
