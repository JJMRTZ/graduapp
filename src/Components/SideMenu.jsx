import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Typography from '@material-ui/core/Typography';
import ReceiptIcon from '@material-ui/icons/Receipt';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import ShowChartIcon from '@material-ui/icons/ShowChart';
import { useHistory } from "react-router-dom";

const useStyles = makeStyles(theme=>({
    list: {
      width: 250,
      height:"100%",
      boxShadow:"2px 0 5px -2px #888;",
      overflow:"hidden",
      zIndex:"1",
      [theme.breakpoints.down(750)]:{
          transition:"0.7s",
          width:"0px",   
      }
      
    },
    fullList: {
      width: 'auto',
    },
  }));

export default function SideMenu(props){
    const classes =useStyles();
    const history=useHistory();
    return(
        <div
        className={classes.list}
        role="presentation"
        >
            <Typography style={{fontSize:"larger",fontWeight:"bold",marginTop:"1vh",marginLeft:"6%"}}>
                Admin
            </Typography>
            <Typography color="textSecondary" style={{fontSize:"medium",fontWeight:"400px",marginTop:".5vh",marginLeft:"6%"}}>
                Administrador
            </Typography>
            <Divider />
            <List style={{fontWeight:"bold"}}>
                <ListItem button key="Solicitudes" style={props.select==="solicitudes"? {backgroundColor:"#2196F3",color:"white"}:{}} onClick={e=> history.push("/Admin")}>
                    <ListItemIcon ><ReceiptIcon  style={props.select==="solicitudes"? {color:"white"}:{}}/></ListItemIcon>
                    <ListItemText primary="solicitudes" />
                </ListItem>
                <ListItem button key="Estadísticas" style={props.select==="estadisticas"? {backgroundColor:"#2196F3",color:"white"}:{}} onClick={e=> history.push("/Charts")}>
                    <ListItemIcon><ShowChartIcon style={props.select==="estadisticas"? {color:"white"}:{}}/></ListItemIcon>
                    <ListItemText primary="Estadisticas" />
                </ListItem>
                <ListItem button key="Cerrar Sesión" onClick={e => history.push("/") }>
                    <ListItemIcon><ExitToAppIcon/></ListItemIcon>
                    <ListItemText primary="Cerrar Sesión" />
                </ListItem>
            </List>
         </div>
    );
}