import React from 'react';
import Login from './Login/Login';
import Register from './Register/Register';
import Admin from './Admin/Admin';
import Subir from './Components/Web-home'
import Charts from './Charts/Charts'
import Invitacion from './Components/Web-home1'
import Procesar from './Components/MenuLateral'

import { Route, BrowserRouter as Router, Switch  } from 'react-router-dom';

export default function App() {
  return(
      <Router>
          <Switch>
              <Route exact path="/"><Login/></Route>
              <Route exact path="/Regist"><Register/></Route>
              <Route exact path="/Admin"><Admin/></Route>
              <Route exact path="/Charts"><Charts/></Route>
              <Route exact path="/Subir"><Subir nombre="Gustavo" apellidos="Garcia" tipoUsuario="Graduado"/></Route>
              <Route exact path="/Invitacion"><Invitacion nombre="Gustavo" apellidos="Garcia" tipoUsuario="Graduado"/></Route>
              <Route exact path="/Procesando"><Procesar nombre="Gustavo" apellidos="Garcia" tipoUsuario="Graduado"/></Route>
              <Route render={() => <h1>404 Error</h1>} />
          </Switch>
      </Router>
  );
}


