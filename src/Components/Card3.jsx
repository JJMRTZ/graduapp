import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import '../Styles/Card2.css'
import Selector from './Selector'


const useStyles = makeStyles({
  root: {
    marginTop:"7%",
    marginLeft:"30%",
    marginRight:"30%",
    width: "50%",
    fontWeight: "bold",
    textAlign: "center",

  },
  pos: {
    marginBottom: "3%",
  },
});

export default function OutlinedCard(props) {
  const classes = useStyles();
  return (
    <Card className={classes.root} variant="outlined">
      <CardContent>
        
        <Typography variant="h5" component="h2">
          {"Hola " + props.nombre}
        </Typography>
        <Typography className={classes.pos} color="textSecondary">
          Tu informacion de pago aun no ha sido registrada, favor
          de subir tu comprobante de pago a continuacion.
        </Typography>
         <div className="selector">
             <Selector />
         </div>
        
      </CardContent>
      
    </Card>
  );
}