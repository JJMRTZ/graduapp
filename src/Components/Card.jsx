import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';

const useStyles = makeStyles({
  root: {
    marginTop:"7%",
    marginLeft:"30%",
    marginRight:"30%"

  },
  pos: {
    marginBottom: "3%",
  },
});

export default function OutlinedCard(props) {
  const classes = useStyles();

  return (
    <Card className={classes.root} variant="outlined">
      <CardContent>
        
        <Typography variant="h5" component="h2">
          {props.nombre}
        </Typography>
        <Typography className={classes.pos} color="textSecondary">
          Tu comprovante fue enviado con exito, espera a que sea procesado.
        </Typography>
        
      </CardContent>
      
    </Card>
  );
}