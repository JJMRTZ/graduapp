import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import '../Styles/Card2.css'
import jsPDF from 'jspdf';


const useStyles = makeStyles({
  root: {
    marginTop:"7%",
    marginLeft:"30%",
    marginRight:"30%",
    width: "50%",
    fontWeight: "bold",
    textAlign: "center",

  },
  pos: {
    marginBottom: "3%",
  },
});

export default function OutlinedCard(props) {
  const classes = useStyles();

  const handlepdf = () =>{
    var doc = new jsPDF();
    doc.text(50, 15, 'INSTITUTO TECNOLOGICO DE MORELIA');
    doc.text(65, 30, 'CEREMONIA DE GRADUCACION');
    doc.text(25, 50, 'Le brindamos el honor a usted '+ props.nombre);
    doc.text(25, 56, 'de presentarse en la ceremonia de graduacion la cual se');
    doc.text(25, 62, 'llevara acabo en el auditorio del instituto tecnologico de morelia');
    doc.text(25, 68, 'en el horario de '+ 'Horario asignado del alumno');
    doc.text(25, 80, 'PROGRAMA: ');
    doc.text(25, 90, '1.- PRESENTACION DE LOS GRADUADOS');
    doc.text(25, 100, '2.- HONORES A LA BANDERA');
    doc.text(25, 110, '3.- PALABRAS DE NUESTRO DIRECTOR INSTITUCIONAL');
    doc.text(25, 120, '4.- ENTREGA DE CERTIFICADOS');
    doc.text(25, 130, '5.- ENTREGA DE RECONOCIMIENTOS ESPECIALES');
    doc.text(25, 140, '6.- DISCURSO DE GRADUACION');
    doc.text(25, 150, '7.- PRESENTACION DEL VIDEO ECHO POR LOS ALUMNOS');
    doc.text(25, 160, '8.- ENTREGA DE DIPLOMAS A LOS ALUMNOS DESTACADOS');
    doc.text(25, 170, '9.- CIERRE Y DESPEDIDA');

    doc.text(25, 190, 'De antemano le prindamos a usted'+'nombre del alumno completo');
    doc.text(25, 200, 'cuya carrera terminada es'+'nombre complento de la carrera');
    doc.text(25, 210, 'muchas felicidades por su logro deseandole asi que continue ');
    doc.text(25, 220,  'y siga aspirando a mas logros, lo esperamos en la ceremonia');

    var logo = new Image();
    logo.src = 'img_home.jpg'
    doc.addImage(logo, 'JPEG', 55, 225, 100, 50);
    
    doc.text(25, 280, 'LA EDUCACION ES EL ARMA MAS PODEROSA QUE PUEDES');
    doc.text(55, 290, 'USAR PARA CAMBIAR LE MUNDO');

    doc.save('prueba.pdf');
}

  return (
    <Card className={classes.root} variant="outlined">
      <CardContent>
        
        <Typography variant="h5" component="h2">
          {"Hola " + props.nombre}
        </Typography>
        <Typography className={classes.pos} color="textSecondary">
          Tu comprovante de pago fue procesado y aceptado puedes
          descargar tu invitacion las veces que quieras.
        </Typography>
        <button  onClick={handlepdf} className="btn-Descargar">
          Descargar Invitacion
        </button>
      </CardContent>
      
    </Card>
  );
}