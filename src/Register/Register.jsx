import React, { useState} from 'react';
import {useHistory} from 'react-router-dom';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import FilledInput from '@material-ui/core/FilledInput';
import InputLabel from '@material-ui/core/InputLabel';
import InputAdornment from '@material-ui/core/InputAdornment';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import IconButton from '@material-ui/core/IconButton';
import FormControl from '@material-ui/core/FormControl';
import FormHelperText from '@material-ui/core/FormHelperText';
import Button from '@material-ui/core/Button';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import clsx from 'clsx';
import HomeImage from '../Images/img_home.jpg';
import Logo from '../Images/img_logo.png';
import {database} from "../firebase";
import Modal from '../Components/Modal'
import '../Styles/registro.css'


const useStyles = makeStyles(theme => ({
    root: {
        display: "flex",
        flexDirection: "row",
        width: "100%",
        height: "100vh",
    },
    izquierda: {
        width: "50%",
        backgroundImage: `linear-gradient(
            rgba(25,118,210, .7),
            rgba(25,118,210, .7)
          ),url(${HomeImage})`,
        height: "100%",
        backgroundRepeat: "no-repeat",
        backgroundSize: "cover",
        alignContent: "center",
        justifyContent: "center",
        display: "flex",
        flexDirection: "column",
        [theme.breakpoints.down(750)]:{
            display:"none",
        }
    },
    derecha: {
        width: "50%",
        backgroundColor: "white",
        height: "100%",
        alignContent: "center",
        justifyContent: "center",
        display: "flex",
        flexDirection: "column",
        [theme.breakpoints.down(750)]:{
            width:"100%",
        }
    },
    margin: {
        margin: "auto",
        width: "100%",
        alignSelf:"center"
    },
    imgres:{
        display:"none",
        width:"25%",
        alignSelf: "center",
        [theme.breakpoints.down(750)]:{
            display:"block",
        }
    },
    btnres:{
        backgroundColor:"#FFC107",
        color:"white",
        marginTop:"1vh",
        width:"40%",
        alignSelf:"center",
        marginBottom:"8vh",
        display:"none", 
        [theme.breakpoints.down(750)]:{
            display:"block",
        } 
    },
    formulario:{
        width: "60%",
        display:"flex",
        alignSelf: "center",
        [theme.breakpoints.down(750)]:{
           width:"80%",
        } 
    }
}));

export default function Register() {
    const classes = useStyles();
    const history = useHistory();
   const [values, setValues] = useState({
        showPassword: false,
        showPassword2: false,
    });

    const [State, setState] = useState({
        nombre: {
            value: '',
            required: true,
            error: false,
            label: "Nombre(s)",
          },
          paterno: {
            value: '',
            required: true,
            error: false,
            label: "Apeido Paterno",
          },
          materno: {
            value: '',
            required: true,
            error: false,
            label: "Apeido materno",
          },
          correo: {
            value: '',
            required: true,
            error: false,
            label: "Correo",
          },
          carrera: {
            value: '',
            required: true,
            error: false,
            label: 'Elija una Carrera',
            name: 'carrera',
          },
          control: {
            value: '',
            required: true,
            error: false,
            label: 'Numero de Control'
          },
          ModalRegistro: false,
          password1: {
            value: '',
            required: true,
            error: false,
            label: 'Contraseña'
          },
          password2: {
            value: '',
            required: true,
            error: false,
            label: 'Contraseña'
          },
  
      });

    const [carreras] = useState({
        Ingenierias: {
         Sistemas: 'Sistemas Computacionales',
         Industrial: 'Industrial',
         Gestion: 'Gestion Empresarial',
         Bio: 'Bioquimica',
         electrica: 'Electrica',
         electronica: 'Electronica',
         mecanica: 'Mecanica',
         mecatro: 'Mecatronica',
         }
        });

    const checkRegExp = (obj, regExp, label, labelDefault, size) => {
        if (obj.value === null) {
            obj.value = '';
        }
        var longitud;
        var expresion;
  
        obj.error = (obj.value.length > size);
        longitud = obj.error;
        obj.error = !(regExp.test(obj.value));
        expresion = obj.error;
        if (longitud) {
            obj.label = `La longitud maxima son ${size} caracteres`;
            obj.error = true;
        } else if (expresion) {
            obj.label = label;
        } else {
            obj.label = labelDefault;
        }
  
        return obj;
     }

     const checkPass = (obj, obj2, label, labelDefault) =>{
        if(obj.value !== obj2.value){
          obj.label = label;
          obj.error = true;
        }else{
          obj.label = labelDefault;
          obj.error = false;
        }

    return obj;
 }

    const  handleChange = () => e => {
        let obj = null;
        obj = State.nombre;
        let obj22 = null;
        const { name, value } = e.target;
        switch (name) {
            
            case 'Nombre':
              obj.value = value; 
              obj = checkRegExp(obj,/^[a-zA-Z ]*$/,'Nombre no valido','Nombre',10);
              setState( {...State, nombre: obj})
            break;

            case 'Correo':
              obj = State.correo;
              obj.value = value; 
              obj = checkRegExp(obj,
                  /^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/,
                  'Correo no valido',
                  'Correo');
              setState({...State, correo: obj });
              break;

              case 'Paterno':
              obj = State.paterno;
              obj.value = value; 
              obj = checkRegExp(obj,
                   /^[a-zA-Z ]*$/,
                  'Apeido no valido',
                  'Apeido Paterno',10);
              setState({...State, paterno: obj });
              break;

              case 'Materno':
              obj = State.materno;
              obj.value = value; 
              obj = checkRegExp(obj,
                   /^[a-zA-Z ]*$/,
                  'Apeido no valido',
                  'Apeido Materno',10);
              setState({...State, materno: obj });
              break;
                
                case 'Control':
                obj = State.control;
                obj.value = value;
                obj = checkRegExp(obj,
                  /^([0-9]){8}/,
                 'Numero no valido',
                 'Numero de control',10);
                setState({...State, control: obj });
                break;

                case 'Carrera':
                obj = State.carrera;
                obj.value = value; 
                setState({...State, carrera: obj });
                break;

                case 'Password':
                obj = State.password1;
                obj.value = value; 
                setState({...State, password1: obj });
                break;

                case 'Password2':
                obj = State.password2;
                obj22 = State.password1;
                obj.value = value; 
                obj = checkPass(obj, obj22,'Contraseña no coincide', 'Contraseña');
                setState({...State, password2: obj });
                break;
                default:
                break
        }
    }

    const handleClickShowPassword = () => {
        setValues({ ...values, showPassword: !values.showPassword });
    };

    const handleClickShowPassword2 = () => {
        setValues({ ...values, showPassword2: !values.showPassword2 });
    };

    const handleMouseDownPassword = event => {
        event.preventDefault();
    };
    const handleSubmit = e => {
        e.preventDefault();

        var referencia = database.ref('/estudiantes/'+ State.control.value);
           referencia.update({
              ap_m: State.materno.value,
              ap_p: State.paterno.value,
              carrera: State.carrera.value,
              contraseña: State.password1.value,
              nombre: State.nombre.value,
              solicitud: 'null',
            }); 

                setState({ 
                    ...State,
                    ModalRegistro: true,
                });
              
    }

    const vacio = () =>{
        if(State.nombre.value === ''){
            State.nombre.error = true;
        }
        if(State.paterno.value === ''){
          State.paterno.error = true;
        }
        if(State.materno.value === ''){
          State.materno.error = true;
        }
        if(State.correo.value === ''){
          State.correo.error = true;
        }
        if(State.carrera.value === ''){
          State.carrera.error = true;
        }else{
          State.carrera.error = false;
        }
        if(State.control.value === ''){
          State.control.error = true;
        }
        if(State.password2.value === ''){
          State.password2.error = true;
        }

      }

      const handleCloseModal = e =>{
        setState({ 
            ...State,
            ModalRegistro: false,
        });

        history.push("/");
      }

    return (
        <div className={classes.root}>
             {vacio()}
            <div className={classes.izquierda}>
                <img src={Logo} alt="" width="25%" style={{ alignSelf: "center"}} />
                <Typography style={{ fontSize: "larger", textAlign: "center",color:"white",marginTop:"1vh"}}>
                    GraduApp
                </Typography>
                <Typography style={{ fontSize: "large", textAlign: "center",color:"white",marginTop:"8vh"}}>
                    ¿Ya tienes cuenta?<br/>
                    Inicia Sesión para atender tu  <br/>
                    solicitud
                </Typography>
                <Button variant="contained" style={{backgroundColor:"#FFC107",color:"white",marginTop:"1vh",width:"46%",alignSelf:"center",marginBottom:"8vh"}} >
                       Iniciar Sesión
                </Button>
            </div>
            <div className={classes.derecha}>
                <img src={Logo} alt="" className={classes.imgres}/>
                <Typography style={{ fontWeight: "bold", fontSize: "larger", textAlign: "center" }}>
                    Registrar
             </Typography>
                <Typography style={{
                    fontSize: "large",
                    textAlign: "center",
                    marginBottom: "4vh",
                }}
                    color="textSecondary">
                    Ingresa los siguientes datos para crear una cuenta
             </Typography>
                <form className={classes.formulario} onSubmit={handleSubmit} id="formulario">
                    <div style={{width:"50%",display:"flex",flexDirection:"column",marginRight:"2%"}}>
                    <TextField
                        label={State.nombre.label}
                        name="Nombre"
                        value={State.nombre.value}
                        helperText="Ingresa Nombre(s)"
                        variant="filled"
                        style={{
                            width: "100%",
                            alignSelf:"center",
                            marginBottom:"2%"
                        }}
                        onChange={handleChange()}
                    />
                     <TextField
                        label={State.materno.label}
                        helperText="Ingresa Apellido materno"
                        variant="filled"
                        name="Materno"
                        value={State.materno.value}
                        style={{
                            width: "100%",
                            alignSelf:"center",
                            marginBottom:"2%"
                        }}
                        onChange={handleChange()}
                    />
                    <FormControl variant="filled" className={classes.formControl} style={{marginBottom:"2%"}}>
                            <InputLabel id="demo-simple-select-filled-label">Carrera</InputLabel>
                            <Select
                                labelId="demo-simple-select-filled-label"
                                id="demo-simple-select-filled"
                                name="Carrera"
                                value={State.carrera.value}
                                onChange={handleChange()}
                            >   
                                {Object.entries(carreras.Ingenierias).map((item, index) => {
                                return(
                                        <MenuItem key={index} value={item[1]}>{item[1]}</MenuItem>
                                     ); 
                                    
                                 })}
                            </Select>
                            <FormHelperText id="standard-weight-helper-text">Seleccione su carrera</FormHelperText>
                    </FormControl>
                    <FormControl className={clsx(classes.margin, classes.textField)} variant="filled">
                        <InputLabel htmlFor="filled-adornment-password">Password</InputLabel>
                        <FilledInput
                            type={values.showPassword ? 'text' : 'password'}
                            value={State.password1.value}
                            name="Password"
                            onChange={handleChange()}
                            endAdornment={
                                <InputAdornment position="end">
                                    <IconButton
                                        aria-label="toggle password visibility"
                                        onClick={handleClickShowPassword}
                                        onMouseDown={handleMouseDownPassword}
                                        edge="end"
                                    >
                                        {values.showPassword ? <Visibility /> : <VisibilityOff />}
                                    </IconButton>
                                </InputAdornment>
                            }
                        />
                        <FormHelperText id="standard-weight-helper-text">Ingresa la contraseña</FormHelperText>
                    </FormControl>
                    </div>
                    <div style={{width:"50%",display:"flex",flexDirection:"column"}}>
                    <TextField
                        label={State.paterno.label}
                        name="Paterno"
                        value={State.paterno.value}
                        helperText="Ingresa Apellido paterno"
                        variant="filled"
                        style={{
                            width: "100%",
                            alignSelf:"center",
                            marginBottom:"2%"
                        }}
                        onChange={handleChange()}
                    />
                    <TextField
                        label={State.control.label}
                        helperText="Ingresa No de control"
                        variant="filled"
                        name="Control"
                        value={State.control.value}
                        style={{
                            width: "100%",
                            alignSelf:"center",
                            marginBottom:"2%"
                        }}
                        onChange={handleChange()}
                    />
                    <TextField
                        label={State.correo.label}
                        helperText="Ingresa correo electrónico"
                        variant="filled"
                        name="Correo"
                        value={State.correo.value}
                        style={{
                            width: "100%",
                            alignSelf:"center",
                            marginBottom:"2%"
                        }}
                        onChange={handleChange()}
                    />
                    <FormControl className={clsx(classes.margin, classes.textField)} variant="filled">
                    <InputLabel htmlFor="filled-adornment-password">{State.password2.label}</InputLabel>
                        <FilledInput
                            type={values.showPassword2 ? 'text' : 'password'}
                            value={State.password2.value}
                            name="Password2"
                            onChange={ handleChange() }
                            endAdornment={
                                <InputAdornment position="end">
                                    <IconButton
                                        aria-label="toggle password visibility"
                                        onClick={handleClickShowPassword2}
                                        onMouseDown={handleMouseDownPassword}
                                        edge="end"
                                    >
                                        {values.showPassword2 ? <Visibility /> : <VisibilityOff />}
                                    </IconButton>
                                </InputAdornment>
                            }
                        />
                        <FormHelperText id="standard-weight-helper-text">Repita la contraseña</FormHelperText>
                    </FormControl>
                    </div>
                </form>
                <Button     
                        variant="contained"  
                        form="formulario" 
                        type="submit" 
                        style={{backgroundColor:"#FFC107",color:"white",marginTop:"1vh",width:"40%",alignSelf:"center"}}
                        disabled={ State.nombre.error || State.paterno.error|| State.materno.error
                            || State.control.error || State.carrera.error || State.correo.error
                            || State.password2.error }
                         >
                        Registrar
                 </Button>
                 
                
                <Button variant="contained" className={classes.btnres} onClick={e =>history.push("/") }>
                        Iniciar Sesión
                </Button>
                
                <Modal 
                 isOpen={State.ModalRegistro} 
                 onClose={handleCloseModal}>     
                  <div className="contenedor-11">
                    <button  onClick={handleCloseModal} className="btn-Enviar11" handleChange={handleCloseModal} >
                        Aceptar
                    </button>
                  </div>  
                </Modal>
                
            </div>
        </div>
    );
}