import React from 'react';
import Card3 from './Card3'
import { makeStyles } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import AppBar from '@material-ui/core/AppBar';
import CssBaseline from '@material-ui/core/CssBaseline';
import Toolbar from '@material-ui/core/Toolbar';
import List from '@material-ui/core/List';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import AttachMoneyIcon from '@material-ui/icons/AttachMoney';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import '../Styles/Menulateral.css';
import {useHistory} from 'react-router-dom';
const drawerWidth = "250px";

const useStyles = makeStyles(theme => ({
  root:{
    display: 'flex',

  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
    backgroundColor:"#1976D2"
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
    
  },
  drawerPaper: {
    width: drawerWidth,
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
    textSizeAdjust: drawerWidth
  },
  infoUsuario:{
    marginRight: theme.spacing(2),
    paddingTop: 0,
    paddingBottom:0
    
  },
  toolbar: theme.mixins.toolbar,
}));

export default function ClippedDrawer(props) {
  const classes = useStyles();
  const [value]=React.useState(JSON.parse(localStorage.getItem('usuario')) );
  const history = useHistory();

  return (
    <div className={classes.root}>
      <CssBaseline />
      <AppBar position="fixed" className={classes.appBar}>
        <Toolbar>
          <IconButton edge="start" className={classes.menuButton} color="inherit" aria-label="menu">
            <MenuIcon />
          </IconButton>
          <Typography variant="h6" noWrap>
            GraduApp
          </Typography>
        </Toolbar>
      </AppBar>
      <Drawer
        className={classes.drawer}
        variant="permanent"
        classes={{
          paper: classes.drawerPaper,
        }}
      >
        <div className={classes.toolbar} />
        <List>
            <ListItem className={classes.infoUsuario}>
             <ListItemText className="user" primary={value.nombre}/> 
            </ListItem>
            <ListItem className={classes.infoUsuario}>
              <ListItemText primary="Graduado"/>
            </ListItem>
        </List>
        <Divider />
        <List>
            <ListItem button key={'Comprovante'}>
              <ListItemIcon><AttachMoneyIcon/></ListItemIcon>
              <ListItemText primary={'Comprobante'} />
              
            </ListItem>
            <ListItem button key={'Cerrar Sesion'}>
            <ListItemIcon><ExitToAppIcon /></ListItemIcon>
            <ListItemText primary={'Cerrar Session'} onClick= {e=>{history.push("/")}}/>
          </ListItem>
        </List>
      </Drawer>
      <main className={classes.content}>
        <Card3 nombre={value.nombre}/>
      </main>
    </div>
  );
}