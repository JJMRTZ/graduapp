import React from 'react';
import firebase from 'firebase';
import '../Styles/selector.css'
import { withRouter } from "react-router-dom";
import {database} from '../firebase';
class ImagePicker extends React.Component {
    constructor(props) {
      super(props);
   
      this.state = {
        data: null,
        fullscreen: false,
        image:null,
        loading: false
      }; 
      this.handleFileChange = this.handleFileChange.bind(this);
      this.handlePreviewClick = this.handlePreviewClick.bind(this);
      this.handleClearClick = this.handleClearClick.bind(this);
    }
  
    handleFileChange(event) {
      const {target} = event;
      const {files} = target;
  
      if (files && files[0]) {
        var reader = new FileReader();
        
        reader.onloadstart = () => this.setState({loading: true});
        reader.onload = event => {
          this.setState({
            data: event.target.result,
            loading: false
          });
          const storageRef= firebase.storage().ref(`/Comprobantes/${files[0].name}`);
          const task=storageRef.put(files[0]);
          task.on('state_changed', function(snapshot){
            var progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
            console.log('Upload is ' + progress + '% done');
          }, function(error) {
            console.log(error.message)
          },()=>{
            storageRef.getDownloadURL().then(url=>{
              this.setState({
                image:url
              });
              console.log(this.state.image);
          }); 
          })
        };
     
        reader.readAsDataURL(files[0]);
      }
    }
    
    handleClearClick() {
      this.setState({
        data: null,
        fullScreen: false
      });
    }
    handleUpload= ()=> {
      const values=JSON.parse(localStorage.getItem('usuario'))
      
      console.log(this.state.image)
      console.log(values.id)
     var newPostRef=database.ref("/solicitudes").push({
        estado:0,
        foto:this.state.image,
        n_control:values.id,
        notas:"null"
      })
      var postID = newPostRef.key;
      database.ref(`/estudiantes`).child(`${values.id}`).update({
        'solicitud':postID
      }).then(()=>{
        this.props.history.push('/Procesando')
      })
    }
    handlePreviewClick() {
      const {data, fullScreen} = this.state;
   
      if (!data) {
        return;
      }
      
      this.setState({fullScreen: !fullScreen});
    }
  
    render() {
      const {data, loading} = this.state;
      const backgroundImage = data ? {backgroundImage: `url(${data})`} : null;
      
            
  
      return (
        <div>
  
          <input
            id="car"
            type="file"
            accept="image/*"
            capture="camera"
            onChange={this.handleFileChange}
          />
  
          <div
            className="preview"
            style={backgroundImage}
            onClick={this.handlePreviewClick}
          >
            {!data && !loading &&
              <label htmlFor="car">
                Click to capture
              </label>
            }
  
            {loading &&
              <span>Loading...</span>
            }
          </div>
          
          <button type='button' className="clearButton" onClick={this.handleClearClick}>
            Borrar Imagen
          </button>
          <button className="btn-Subir" type='button' onClick={this.handleUpload} disabled={(this.state.image ? false:true) || (this.state.data ? false:true) }>
          Subir Comprobante
          </button>
        </div>
      );
    }
  }

export default withRouter(ImagePicker);